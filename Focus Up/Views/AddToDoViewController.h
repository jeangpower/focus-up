//
//  AddToDoViewController.h
//  Focus Up
//
//  Created by Power, Jean on 11/17/20.
//  Copyright © 2020 Power, Jean. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AddToDoViewControllerDelegate.h"

@interface AddToDoViewController : UIViewController<UITextViewDelegate>

@property (weak) id <AddToDoViewControllerDelegate> _Nullable delegate;

@end

