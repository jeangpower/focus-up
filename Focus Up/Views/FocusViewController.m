//
//  FocusViewController.m
//  Focus Up
//
//  Created by Power, Jean on 11/6/20.
//  Copyright © 2020 Power, Jean. All rights reserved.
//

#import "FocusViewController.h"
#import "AddToDoViewController.h"
#import "ToDoListTableViewCell.h"
#import "IconService.h"
#import "ToDosViewModel.h"

@interface FocusViewController ()

@property (weak, nonatomic) IBOutlet UITableView *todoList;
@property (weak, nonatomic) IBOutlet UIButton *add;

@property IconService *iconService;
@property ToDosViewModel *toDosViewModel;

@end

@implementation FocusViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    self.todoList.dataSource = self;
    self.todoList.delegate = self;
    
    [self setUpAppData];
    [self setUpAddButton];
}

- (void)setUpAppData {
    self.iconService = [IconService new];
    [self.iconService prefetchIcons];
    self.toDosViewModel = [[ToDosViewModel alloc] init];
    self.toDosViewModel.delegate = self;
}

- (void)setUpAddButton {
    UIImage *untintedImage = [self.iconService imageForIcon:IconTypeAdd];
    [self.add setBackgroundImage:[untintedImage imageWithTintColor:self.view.tintColor] forState:UIControlStateNormal];
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return [self.toDosViewModel numberOfToDos];
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    ToDoListTableViewCell *cell = [self.todoList dequeueReusableCellWithIdentifier:@"ToDoCell"];
    NSString *toDoText = [self.toDosViewModel toDoTextAtPosition:indexPath.row];
    
    if (toDoText != nil) {
        BOOL toDoComplete =  [self.toDosViewModel toDoCompletionAtPosition:indexPath.row];
        UIImage *untintedImageForCell = (toDoComplete) ? [self.iconService imageForIcon:IconTypeChecked] : [self.iconService imageForIcon:IconTypeUnchecked];
        
        cell.toDoText.text = toDoText;
        [cell.checkBox setImage:[untintedImageForCell imageWithTintColor:self.view.tintColor]];
    }
    
    return cell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    [self.toDosViewModel toggleCompletionOfToDoAtPosition:indexPath.row];
}

- (void)tableView:(UITableView *)tableView commitEditingStyle:(UITableViewCellEditingStyle)editingStyle forRowAtIndexPath:(NSIndexPath *)indexPath {
    if (editingStyle == UITableViewCellEditingStyleDelete) {
        [self.toDosViewModel deleteToDoAtPosition:indexPath.row];
        [self.todoList deleteRowsAtIndexPaths:@[indexPath] withRowAnimation:UITableViewRowAnimationAutomatic];
    }
}

- (CGFloat)tableView:(UITableView *)tableView estimatedHeightForRowAtIndexPath:(NSIndexPath *)indexPath {
    return UITableViewAutomaticDimension;
}

- (IBAction)addToDo:(UIButton *)sender {
    UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
    AddToDoViewController *addToDoViewController = [storyboard instantiateViewControllerWithIdentifier:@"AddToDoViewController"];
    addToDoViewController.delegate = self;
    addToDoViewController.modalPresentationStyle = UIModalPresentationOverCurrentContext;
    addToDoViewController.modalTransitionStyle = UIModalTransitionStyleCoverVertical;
    [self presentViewController:addToDoViewController animated:YES completion:nil];
}

- (void)toDoAdded:(NSString *) toDo {    
    if (toDo.length > 0) {
        [self.toDosViewModel addToDo:toDo];
    }
}

- (void)updateComplete {
    [self.todoList reloadData];
}

@end
