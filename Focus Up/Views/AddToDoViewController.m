//
//  AddToDoViewController.m
//  Focus Up
//
//  Created by Power, Jean on 11/17/20.
//  Copyright © 2020 Power, Jean. All rights reserved.
//

#import "AddToDoViewController.h"
#import "Constants.h"

@interface AddToDoViewController()

@property (weak, nonatomic) IBOutlet UITextView *toDoTextEntry;

@end

@implementation AddToDoViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    self.toDoTextEntry.delegate = self;
    
    [self setUpTextViewPlaceholder];
    [self setUpTapToDismissKeyboard];
}

- (void)setUpTextViewPlaceholder {
    self.toDoTextEntry.text = placeholder;
    self.toDoTextEntry.textColor = UIColor.lightGrayColor;
}

- (void)setUpTapToDismissKeyboard {
    UITapGestureRecognizer *tap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(dismissKeyboard)];
    [self.view addGestureRecognizer:tap];
}

-(void)dismissKeyboard {
    [self.toDoTextEntry resignFirstResponder];
}

- (void)textViewDidBeginEditing:(UITextView *)textView {
    // Check to prevent user added text being deleted
    if ([self.toDoTextEntry.text isEqualToString:placeholder]) {
        self.toDoTextEntry.text = nil;
        self.toDoTextEntry.textColor = UIColor.blackColor;
    }
}

- (IBAction)cancel:(UIButton *)sender {
    [self dismissViewControllerAnimated:YES completion:nil];
}

- (IBAction)addToDo:(UIButton *)sender {
    // Return user added text, not the placeholder
    if (![self.toDoTextEntry.text isEqualToString:placeholder]) {
        [self.delegate toDoAdded:self.toDoTextEntry.text];
    }
    [self dismissViewControllerAnimated:YES completion:nil];
}

@end

