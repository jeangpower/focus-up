//
//  FocusViewController.h
//  Focus Up
//
//  Created by Power, Jean on 11/6/20.
//  Copyright © 2020 Power, Jean. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AddToDoViewControllerDelegate.h"
#import "ToDosViewModelDelegate.h"

@interface FocusViewController : UIViewController<UITableViewDataSource, UITableViewDelegate, AddToDoViewControllerDelegate, ToDosViewModelDelegate>


@end

