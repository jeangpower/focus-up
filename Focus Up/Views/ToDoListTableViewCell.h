//
//  ToDoListTableViewCell.h
//  Focus Up
//
//  Created by Power, Jean on 11/12/20.
//  Copyright © 2020 Power, Jean. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ToDoListTableViewCell : UITableViewCell

@property (weak, nonatomic) IBOutlet UILabel *toDoText;
@property (weak, nonatomic) IBOutlet UIImageView *checkBox;

@end
