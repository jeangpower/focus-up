//
//  Constants.h
//  Focus Up
//
//  Created by Power, Jean on 11/13/20.
//  Copyright © 2020 Power, Jean. All rights reserved.
//

#import <Foundation/Foundation.h>

typedef NS_ENUM(NSInteger, IconType) {
        IconTypeChecked,
        IconTypeUnchecked,
        IconTypeAdd,
};

// Icon identifiers
extern NSString * const addIdentifier;
extern NSString * const checkedIdentifier;
extern NSString * const uncheckedIdentifier;

// Icon URLs
extern NSString * const addIconURL;
extern NSString * const checkedIconURL;
extern NSString * const uncheckedIconURL;

// ToDo static text
extern NSString * const placeholder;
