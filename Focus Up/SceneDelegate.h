//
//  SceneDelegate.h
//  Focus Up
//
//  Created by Power, Jean on 11/6/20.
//  Copyright © 2020 Power, Jean. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface SceneDelegate : UIResponder <UIWindowSceneDelegate>

@property (strong, nonatomic) UIWindow * window;

@end

