//
//  NetworkService.h
//  Focus Up
//
//  Created by Power, Jean on 11/14/20.
//  Copyright © 2020 Power, Jean. All rights reserved.
//

@interface NetworkService : NSObject

- (void)fetchDataFromURL:(NSURL *_Nonnull) url completionHandler:(void (^_Nonnull)(NSData *_Nullable data, NSError * _Nullable error)) completionHandler;

@end
