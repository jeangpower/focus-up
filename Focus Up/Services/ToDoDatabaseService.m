//
//  ToDoDatabaseService.m
//  Focus Up
//
//  Created by Power, Jean on 11/15/20.
//  Copyright © 2020 Power, Jean. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "ToDoDatabaseService.h"
#import <Realm/Realm.h>

@interface ToDoDatabaseService()

@property RLMRealm *realm;

@end

@implementation ToDoDatabaseService

-(instancetype)init {
    self.realm = [RLMRealm defaultRealm];
    return self;
}

- (void)put:(RealmToDo *) realmToDo {
    [self.realm beginWriteTransaction];
    [self.realm addObject:realmToDo];
    [self.realm commitWriteTransaction];
}

- (RLMResults<RealmToDo *> *)fetchAllSorted {
    return [[RealmToDo allObjects] sortedResultsUsingKeyPath:@"creation" ascending:true];
}
        
- (void)toggleCompletionOfToDo:(RealmToDo *) realmToDo {
    [self.realm beginWriteTransaction];
    // All updates to Realm objects must be within a write transaction.
    realmToDo.completed = !realmToDo.completed;
    [self.realm commitWriteTransaction];
}

- (void)delete:(RealmToDo *) realmToDo {
    [self.realm beginWriteTransaction];
    [self.realm deleteObject:realmToDo];
    [self.realm commitWriteTransaction];
}

@end
