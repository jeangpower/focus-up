//
//  IconService.m
//  Focus Up
//
//  Created by Power, Jean on 11/13/20.
//  Copyright © 2020 Power, Jean. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>
#import "IconService.h"
#import "Icon.h"
#import "NetworkService.h"
#import "StorageService.h"

@interface IconService()

@property NetworkService *networkService;
@property StorageService *storageService;
@property NSArray<Icon *> *icons;

@end

@implementation IconService

- (instancetype)init {
    return [self initWithNetworkService:[[NetworkService alloc] init] storageService:[[StorageService alloc] init]];
}

- (instancetype) initWithNetworkService:(NetworkService *)networkService storageService:(StorageService *) storageService {
    self.storageService = storageService;
    self.networkService = networkService;
    [self setUpIconReference];
    return self;
}

- (void)setUpIconReference {
    // Reference collection for Icons, holding a type, remote location, an identifier and a default placeholder icon.
    self.icons = @[
        [[Icon alloc] initWithIconType:IconTypeAdd url:[NSURL URLWithString: addIconURL] identifier:addIdentifier placeholder:@"plus"],
        [[Icon alloc] initWithIconType:IconTypeChecked url:[NSURL URLWithString: checkedIconURL] identifier:checkedIdentifier placeholder:@"square"],
        [[Icon alloc] initWithIconType:IconTypeUnchecked url:[NSURL URLWithString: uncheckedIconURL] identifier:uncheckedIdentifier placeholder:@"checkmark.square"],
    ];
}

- (void)prefetchIcons {
    // Fetch icons to memory, if they're not already stored
    if (![self allIconsStored]) {
        for (Icon *icon in self.icons) {
            [self.networkService fetchDataFromURL:icon.url completionHandler: ^(NSData *data, NSError *error) {
                if (error == nil) {
                    [self.storageService storeData:data withIdentifier:[self identifierForIcon:icon.iconType]];
                }
                // No-op if error, as placeholders will be used, so no impact on end user.
            }];
        }
    }
}

- (UIImage *)imageForIcon:(IconType)iconType {
    UIImage *image;
    NSData *iconData = [self.storageService fetchDataWithIdentifier:[self identifierForIcon:iconType]];

    if (iconData != nil) {
        image = [UIImage imageWithData:iconData];
    } else {
        image = [UIImage systemImageNamed:[self placeholderForIcon:iconType]];
    }
    return image;
}

- (NSString *)identifierForIcon:(IconType) iconType {
    NSString *identifer;
    
    for (Icon *icon in self.icons) {
        if (icon.iconType == iconType) {
            identifer = icon.identifier;
        }
    }
    return identifer;
}

- (NSString *)placeholderForIcon:(IconType) iconType {
    NSString *placeholder;

    for (Icon *icon in self.icons) {
        if (icon.iconType == iconType) {
            placeholder = icon.placeholder;
        }
    }
    return placeholder;
}

- (BOOL)allIconsStored {
    BOOL allPresent = YES;
    
    for (Icon *icon in self.icons) {
        if (![self.storageService dataPresentForIdentifier:icon.identifier]) {
            allPresent = NO;
        }
    }

    return allPresent;
}

@end
