//
//  IconService.h
//  Focus Up
//
//  Created by Power, Jean on 11/13/20.
//  Copyright © 2020 Power, Jean. All rights reserved.
//

#import "Constants.h"

@interface IconService : NSObject

- (void)prefetchIcons;
- (UIImage * _Nullable)imageForIcon:(IconType) iconType;

@end
