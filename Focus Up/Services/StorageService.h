//
//  StorageService.h
//  Focus Up
//
//  Created by Power, Jean on 11/14/20.
//  Copyright © 2020 Power, Jean. All rights reserved.
//

@interface StorageService : NSObject

- (void)storeData:(NSData * _Nonnull) data withIdentifier:(NSString * _Nonnull) identifier;
- (NSData * _Nullable)fetchDataWithIdentifier:(NSString * _Nonnull) identifier;
- (BOOL)dataPresentForIdentifier:(NSString * _Nonnull) identifier;

@end
