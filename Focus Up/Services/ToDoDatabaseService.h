//
//  ToDoDatabaseService.h
//  Focus Up
//
//  Created by Power, Jean on 11/15/20.
//  Copyright © 2020 Power, Jean. All rights reserved.
//

#import "RealmToDo.h"

@interface ToDoDatabaseService : NSObject

- (void)put:(RealmToDo *) realmToDo;
- (RLMResults<RealmToDo *> *)fetchAllSorted;
- (void)toggleCompletionOfToDo:(RealmToDo *) realmToDo;
- (void)delete:(RealmToDo *) realmToDo;

@end
