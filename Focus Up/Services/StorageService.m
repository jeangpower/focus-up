//
//  StorageService.m
//  Focus Up
//
//  Created by Power, Jean on 11/14/20.
//  Copyright © 2020 Power, Jean. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "StorageService.h"

@interface StorageService()

@property NSCache<NSString *, NSData *> *sessionCache;
@property NSFileManager *fileManager;
@property NSURL *appCacheDirectory;

@end

@implementation StorageService

- (instancetype)init {
    return [self initWithFileManager:[NSFileManager defaultManager] cache:[[NSCache alloc] init]];
}

- (instancetype)initWithFileManager:(NSFileManager *)fileManager cache:(NSCache *) cache {
    self.fileManager = fileManager;
    self.sessionCache = cache;
    [self createDirectoryInPersistedCache];
    return self;
}

- (void)storeData:(NSData *) data withIdentifier:(NSString *) identifier {
    [self.sessionCache setObject:data forKey:identifier];
    
    dispatch_async(dispatch_get_global_queue(QOS_CLASS_USER_INITIATED, 0), ^{
        [data writeToFile:[self filePathFromIdentifier:identifier] atomically:YES];
    });
}

- (NSData *)fetchDataWithIdentifier:(NSString * _Nonnull) identifier {
    NSData *result = [self.sessionCache objectForKey:identifier];
    
    if (result == nil) {
        result = [self.fileManager contentsAtPath:[self filePathFromIdentifier:identifier]];
    }
    
    return result;
}

- (BOOL)dataPresentForIdentifier:(NSString * _Nonnull) identifier {
    return ([self.sessionCache objectForKey:identifier] != nil || [self.fileManager fileExistsAtPath:[self filePathFromIdentifier:identifier]]);
}

- (void)createDirectoryInPersistedCache {
    NSArray<NSURL *> *potentialDirectories = [self.fileManager URLsForDirectory:NSCachesDirectory inDomains:NSUserDomainMask];

    if ([potentialDirectories count] > 0) {
        self.appCacheDirectory = [potentialDirectories.firstObject URLByAppendingPathComponent:[[NSBundle mainBundle] bundleIdentifier]];
        
        if (![self.fileManager fileExistsAtPath:self.appCacheDirectory.path]) {
            [self.fileManager createDirectoryAtURL:self.appCacheDirectory withIntermediateDirectories:YES attributes:nil error:nil];
        }
    }
}

- (NSString *)filePathFromIdentifier:(NSString *)identifier {
    NSString *fileName = [identifier stringByAppendingString:@".txt"];
    return [self.appCacheDirectory URLByAppendingPathComponent:fileName].path;
}

@end
