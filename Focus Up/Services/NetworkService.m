//
//  NetworkService.m
//  Focus Up
//
//  Created by Power, Jean on 11/14/20.
//  Copyright © 2020 Power, Jean. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "NetworkService.h"

@interface NetworkService()

@property NSURLSession *urlSession;

@end

@implementation NetworkService

- (instancetype)init {
    return [self initWithURLSession:NSURLSession.sharedSession];
}

-(instancetype)initWithURLSession:(NSURLSession *)urlSession {
    self.urlSession = urlSession;
    return self;
}

- (void)fetchDataFromURL:(NSURL *) url completionHandler:(void (^)(NSData *data, NSError *error)) completionHandler {
    NSURLSessionDataTask *task = [self.urlSession dataTaskWithRequest:[NSURLRequest requestWithURL:url] completionHandler:^(NSData * _Nullable data, NSURLResponse * _Nullable response, NSError * _Nullable error) {
        completionHandler(data, error);
    }];

    [task resume];
}

@end
