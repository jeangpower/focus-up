//
//  Constants.m
//  Focus Up
//
//  Created by Power, Jean on 11/13/20.
//  Copyright © 2020 Power, Jean. All rights reserved.
//

#import "Constants.h"

// Icon identifiers
NSString * const addIdentifier = @"Add";
NSString * const checkedIdentifier = @"Checked";
NSString * const uncheckedIdentifier = @"Unchecked";

// Icon URLs
NSString * const addIconURL = @"https://storage.googleapis.com/focus_app/add.png";
NSString * const checkedIconURL = @"https://storage.googleapis.com/focus_app/checked.png";
NSString * const uncheckedIconURL = @"https://storage.googleapis.com/focus_app/unchecked.png";

// ToDo static text
NSString * const placeholder = @"Today I will...";
