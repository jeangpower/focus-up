//
//  Icon.m
//  Focus Up
//
//  Created by Power, Jean on 11/15/20.
//  Copyright © 2020 Power, Jean. All rights reserved.
//

#import "Icon.h"

@implementation Icon

- (instancetype)initWithIconType:(IconType)iconType url:(NSURL *)url identifier:(NSString *) identifier placeholder:(NSString *)placeholder {
    self.iconType = iconType;
    self.identifier = identifier;
    self.url = url;
    self.placeholder = placeholder;
    return self;
}

@end
