//
//  RealmToDo.h
//  Focus Up
//
//  Created by Power, Jean on 11/15/20.
//  Copyright © 2020 Power, Jean. All rights reserved.
//

#import <Realm/Realm.h>

@interface RealmToDo : RLMObject

@property NSString *toDoText;
@property BOOL completed;
@property int creation;

- (instancetype)init NS_UNAVAILABLE;
- (instancetype)initWithToDoText:(NSString *) text;

@end
