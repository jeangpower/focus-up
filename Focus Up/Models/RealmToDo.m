//
//  RealmToDo.m
//  Focus Up
//
//  Created by Power, Jean on 11/15/20.
//  Copyright © 2020 Power, Jean. All rights reserved.
//

#import "RealmToDo.h"

@implementation RealmToDo

- (instancetype)initWithToDoText:(NSString *) text {
    self = [super init];
    self.toDoText = text;
    self.completed = NO;
    // Loses milliseconds, but user is highly unlikely to add 2 to-dos in less than a second.
    self.creation = (int)[[NSDate date] timeIntervalSince1970];
    return self;
}

+ (NSString *)primaryKey {
    return @"creation";
}

@end
