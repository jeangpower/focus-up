//
//  Icon.h
//  Focus Up
//
//  Created by Power, Jean on 11/15/20.
//  Copyright © 2020 Power, Jean. All rights reserved.
//

#import "Constants.h"

@interface Icon : NSObject

@property NSString * identifier;
@property IconType iconType;
@property NSURL *url;
@property NSString *placeholder;

- (instancetype)init NS_UNAVAILABLE;
- (instancetype)initWithIconType:(IconType)iconType url:(NSURL *)url identifier:(NSString *) identifier placeholder:(NSString *) placeholder;

@end
