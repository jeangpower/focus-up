//
//  ToDosViewModel.h
//  Focus Up
//
//  Created by Power, Jean on 11/15/20.
//  Copyright © 2020 Power, Jean. All rights reserved.
//

#import "ToDosViewModelDelegate.h"

@interface ToDosViewModel : NSObject

@property (weak) id <ToDosViewModelDelegate> _Nullable delegate;

- (void)addToDo:(NSString * _Nonnull) toDoText;
- (void)toggleCompletionOfToDoAtPosition:(NSUInteger) position;
- (NSString * _Nullable)toDoTextAtPosition:(NSUInteger) position;
- (BOOL)toDoCompletionAtPosition:(NSUInteger) position;
- (void)deleteToDoAtPosition:(NSUInteger) position;
- (NSUInteger)numberOfToDos;

@end
