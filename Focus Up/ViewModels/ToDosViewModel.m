//
//  ToDosViewModel.m
//  Focus Up
//
//  Created by Power, Jean on 11/15/20.
//  Copyright © 2020 Power, Jean. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <Realm/Realm.h>
#import "RealmToDo.h"
#import "ToDosViewModel.h"
#import "ToDoDatabaseService.h"

@interface ToDosViewModel()

@property ToDoDatabaseService *toDoDatabaseService;
@property RLMResults<RealmToDo *> *allToDos;
@property RLMNotificationToken *notificationToken;

@end

@implementation ToDosViewModel

- (instancetype)init {
    return [self initWithDatabase:[[ToDoDatabaseService alloc] init]];
}

- (instancetype)initWithDatabase:(ToDoDatabaseService *) toDoDatabaseService {
    self.toDoDatabaseService = toDoDatabaseService;
    self.allToDos = [self.toDoDatabaseService fetchAllSorted];
    [self setUpListenerForRealmChanges];
    return self;
}

- (void)setUpListenerForRealmChanges {
    __weak typeof(self) weakSelf = self;
    
    // Changes within Realm automatically update `allToDos`. We listen for changes to this collection to notify the UI to update.
    self.notificationToken = [self.allToDos addNotificationBlock:^(RLMResults<RealmToDo *> *results, RLMCollectionChange *changes, NSError *error) {
        if (!changes) {
            return;
        }
        [weakSelf.delegate updateComplete];
    }];
}

- (void)addToDo:(NSString *)toDoText {
    RealmToDo *toDo = [[RealmToDo alloc] initWithToDoText:toDoText];
    [self.toDoDatabaseService put:toDo];
}

- (void)toggleCompletionOfToDoAtPosition:(NSUInteger) position {
    if (position <= self.allToDos.count) {
        RealmToDo *completedToDo = self.allToDos[position];
        [self.toDoDatabaseService toggleCompletionOfToDo:completedToDo];
    }
}

- (NSString * _Nullable)toDoTextAtPosition:(NSUInteger) position {
    if (position < self.allToDos.count) {
        return self.allToDos[position].toDoText;
    } else {
        return nil;
    }
}

- (BOOL)toDoCompletionAtPosition:(NSUInteger) position {
    if (position < self.allToDos.count) {
        return self.allToDos[position].completed;
    } else {
        return false;
    }
}

- (void)deleteToDoAtPosition:(NSUInteger) position {
    if (position < self.allToDos.count) {
        RealmToDo *completedToDo = self.allToDos[position];
        [self.toDoDatabaseService delete:completedToDo];
    }
}

- (NSUInteger)numberOfToDos {
    return self.allToDos.count;
}

@end
