//
//  ToDosViewModelDelegate.h
//  Focus Up
//
//  Created by Power, Jean on 11/17/20.
//  Copyright © 2020 Power, Jean. All rights reserved.
//

@protocol ToDosViewModelDelegate

- (void)updateComplete;

@end
