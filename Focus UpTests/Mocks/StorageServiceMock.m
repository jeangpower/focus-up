//
//  StorageServiceMock.m
//  Focus UpTests
//
//  Created by Power, Jean on 11/19/20.
//  Copyright © 2020 Power, Jean. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "StorageServiceMock.h"

@interface StorageServiceMock()

@property NSMutableDictionary<NSString *, NSData *> *storedData;

@end

@implementation StorageServiceMock

- (instancetype)init {
    // Do not want to call super to avoid main class setup
    self.storedData = [[NSMutableDictionary alloc] init];
    return self;
}

- (void)storeData:(NSData * _Nonnull) data withIdentifier:(NSString * _Nonnull) identifier {
    [self.storedData setObject:data forKey:identifier];
}

- (NSData * _Nullable)fetchDataWithIdentifier:(NSString * _Nonnull) identifier {
    return self.storedData[identifier];
}

- (BOOL)dataPresentForIdentifier:(NSString * _Nonnull) identifier {
    return (self.storedData[identifier] != nil);
}

@end
