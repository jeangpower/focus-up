//
//  NSURLSessionMock.h
//  Focus UpTests
//
//  Created by Power, Jean on 11/19/20.
//  Copyright © 2020 Power, Jean. All rights reserved.
//

@interface NSURLSessionMock : NSURLSession

@property NSData * _Nullable returnableData;
@property NSURLResponse * _Nullable returnableResponse;
@property NSError * _Nullable returnableError;

- (_Nonnull instancetype)init;

@end
