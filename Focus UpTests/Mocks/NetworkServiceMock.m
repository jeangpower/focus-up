//
//  NetworkServiceMock.m
//  Focus UpTests
//
//  Created by Power, Jean on 11/19/20.
//  Copyright © 2020 Power, Jean. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "NetworkServiceMock.h"

@implementation NetworkServiceMock

- (void)fetchDataFromURL:(NSURL *) url completionHandler:(void (^)(NSData *data, NSError *error)) completionHandler {
    completionHandler(self.returnableData, self.returnableError);
}

@end

