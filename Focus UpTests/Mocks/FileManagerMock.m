//
//  FileManagerMock.m
//  Focus UpTests
//
//  Created by Power, Jean on 11/19/20.
//  Copyright © 2020 Power, Jean. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "FileManagerMock.h"

@implementation FileManagerMock

- (NSData *)contentsAtPath:(NSString *)path {
    return self.dataAtPath;
}

- (BOOL)fileExistsAtPath:(NSString *)path {
    return self.fileExistsAtPath;
}

- (NSArray<NSURL *> *)URLsForDirectory:(NSSearchPathDirectory)directory inDomains:(NSSearchPathDomainMask)domainMask {
    return @[[[NSURL alloc] initWithString:@"www.test.com"]];
}

@end

