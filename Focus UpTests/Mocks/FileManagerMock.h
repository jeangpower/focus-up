//
//  FileManagerMock.h
//  Focus UpTests
//
//  Created by Power, Jean on 11/19/20.
//  Copyright © 2020 Power, Jean. All rights reserved.
//

@interface FileManagerMock : NSFileManager

@property NSData *dataAtPath;
@property BOOL fileExistsAtPath;

@end
