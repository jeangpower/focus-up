//
//  NSURLSessionMock.m
//  Focus UpTests
//
//  Created by Power, Jean on 11/19/20.
//  Copyright © 2020 Power, Jean. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "NSURLSessionMock.h"

@implementation NSURLSessionMock

- (instancetype)init {
    // Contain init deprecation warning in mock
    self = [super init];
    return self;
}

- (NSURLSessionDataTask *)dataTaskWithRequest:(NSURLRequest *)request completionHandler:(void (^)(NSData * _Nullable data, NSURLResponse * _Nullable response, NSError * _Nullable error))completionHandler {
    completionHandler(self.returnableData, self.returnableResponse, self.returnableError);
    return nil;
}

@end
