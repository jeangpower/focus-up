//
//  ToDosViewModelDelegateMock.h
//  Focus UpTests
//
//  Created by Power, Jean on 11/20/20.
//  Copyright © 2020 Power, Jean. All rights reserved.
//

#import "ToDosViewModelDelegate.h"
#import <XCTest/XCTest.h>

@interface ToDosViewModelDelegateMock : NSObject<ToDosViewModelDelegate>

@property BOOL delegateCalled;
@property XCTestExpectation * _Nullable expectation;

- (instancetype)init;
- (instancetype)initWithExpectation:(XCTestExpectation * _Nonnull) expectation;

@end
