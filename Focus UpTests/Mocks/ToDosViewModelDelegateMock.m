//
//  ToDosViewModelDelegateMock.m
//  Focus UpTests
//
//  Created by Power, Jean on 11/20/20.
//  Copyright © 2020 Power, Jean. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "ToDosViewModelDelegateMock.h"

@implementation ToDosViewModelDelegateMock

- (instancetype)init {
    return [super init];
}

- (instancetype)initWithExpectation:(XCTestExpectation *) expectation {
    self = [super init];
    self.expectation = expectation;
    return self;
}

- (void)updateComplete {
    if (self.expectation != nil) {
        [self.expectation fulfill];
    }
    self.delegateCalled = YES;
}

@end
