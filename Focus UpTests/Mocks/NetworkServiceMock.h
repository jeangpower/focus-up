//
//  NetworkServiceMock.h
//  Focus UpTests
//
//  Created by Power, Jean on 11/19/20.
//  Copyright © 2020 Power, Jean. All rights reserved.
//

#import "NetworkService.h"

@interface NetworkServiceMock : NetworkService

@property NSData * _Nullable returnableData;
@property NSError * _Nullable returnableError;

@end
