//
//  ToDosViewModelTests.m
//  Focus UpTests
//
//  Created by Power, Jean on 11/20/20.
//  Copyright © 2020 Power, Jean. All rights reserved.
//

#import <XCTest/XCTest.h>
#import "Realm.h"
#import "ToDosViewModel.h"
#import "RealmToDo.h"
#import "ToDosViewModelDelegateMock.h"

@interface ToDosViewModelTests : XCTestCase

@property ToDosViewModel *sut;

@end

@implementation ToDosViewModelTests

- (void)setUp {
    // As per Realm documentation:
    // Use an in-memory Realm identified by the name of the current test.
    // This ensures that each test can't accidentally access or modify the data
    // from other tests or the application itself, and because they're in-memory,
    // there's nothing that needs to be cleaned up.
    RLMRealmConfiguration *config = [RLMRealmConfiguration defaultConfiguration];
    config.inMemoryIdentifier = self.name;
    [RLMRealmConfiguration setDefaultConfiguration:config];
    self.sut = [[ToDosViewModel alloc] init];
}

- (void)testAddToDoStoresToDo {
    XCTAssertNil([self.sut toDoTextAtPosition:0]);
    [self.sut addToDo:@"I am some test text"];
    XCTAssertEqualObjects(@"I am some test text", [self.sut toDoTextAtPosition:0]);
}

- (void)testToggleCompletionOfPosition {
    [self.sut addToDo:@"I am some test data"];
    XCTAssertFalse([self.sut toDoCompletionAtPosition:0]);
    [self.sut toggleCompletionOfToDoAtPosition:0];
    XCTAssertTrue([self.sut toDoCompletionAtPosition:0]);
}

- (void)testDeleteToDoDeletesToDoIfPresent {
    [self.sut addToDo:@"I am some test todo text for a test"];
    [self.sut deleteToDoAtPosition:0];
    XCTAssertNil([self.sut toDoTextAtPosition:0]);
}

- (void)testNumberOfToDosEqualsNumber {
    XCTAssertEqual(0, [self.sut numberOfToDos]);
    [self.sut addToDo:@"I am some test todo text for a test"];
    XCTAssertEqual(1, [self.sut numberOfToDos]);
    [self.sut deleteToDoAtPosition:0];
    XCTAssertEqual(0, [self.sut numberOfToDos]);
}

- (void)testPositionRemainsConsistentWithAdditions {
    [self addTestDataToRealmDirectly];
    
    XCTAssertEqual(3, [self.sut numberOfToDos]);
    XCTAssertEqualObjects(@"first", [self.sut toDoTextAtPosition:0]);
    XCTAssertEqualObjects(@"second", [self.sut toDoTextAtPosition:1]);
    XCTAssertEqualObjects(@"third", [self.sut toDoTextAtPosition:2]);

    [self.sut addToDo:@"I am some test todo text for a test"];
    XCTAssertEqual(4, [self.sut numberOfToDos]);

    XCTAssertEqualObjects(@"first", [self.sut toDoTextAtPosition:0]);
    XCTAssertEqualObjects(@"second", [self.sut toDoTextAtPosition:1]);
    XCTAssertEqualObjects(@"third", [self.sut toDoTextAtPosition:2]);
    XCTAssertEqualObjects(@"I am some test todo text for a test", [self.sut toDoTextAtPosition:3]);
}

- (void)testDeletionsDoNotLeaveNilPosition {
    [self addTestDataToRealmDirectly];
    
    XCTAssertEqual(3, [self.sut numberOfToDos]);
    XCTAssertEqualObjects(@"first", [self.sut toDoTextAtPosition:0]);
    XCTAssertEqualObjects(@"second", [self.sut toDoTextAtPosition:1]);
    XCTAssertEqualObjects(@"third", [self.sut toDoTextAtPosition:2]);
    
    [self.sut deleteToDoAtPosition:1];
    
    XCTAssertEqualObjects(@"first", [self.sut toDoTextAtPosition:0]);
    XCTAssertEqualObjects(@"third", [self.sut toDoTextAtPosition:1]);
}

- (void)testDelegateIsNotifiedOfAdditions {
    XCTestExpectation *expectation = [self expectationWithDescription:@"Addition"];
    ToDosViewModelDelegateMock *delegate = [[ToDosViewModelDelegateMock alloc] initWithExpectation:expectation];
    self.sut.delegate = delegate;
    
    XCTAssertFalse(delegate.delegateCalled);

    [self.sut addToDo:@"I am some test todo text for a test"];
    
    [self waitForExpectationsWithTimeout:2.0 handler:^(NSError *error) {
        if (error) {
            XCTFail("Did not get notified of addition");
        }
        XCTAssertTrue(delegate.delegateCalled);
    }];
}

- (void)testDelegateIsNotifiedOfDeletions {
    [self addTestDataToRealmDirectly];

    XCTestExpectation *expectation = [self expectationWithDescription:@"Deletion"];
    ToDosViewModelDelegateMock *delegate = [[ToDosViewModelDelegateMock alloc] initWithExpectation:expectation];
    self.sut.delegate = delegate;
    
    [self.sut deleteToDoAtPosition:1];
    
    [self waitForExpectationsWithTimeout:2.0 handler:^(NSError *error) {
        if (error) {
            XCTFail("Did not get notified of deletion");
        }
        XCTAssertTrue(delegate.delegateCalled);
    }];
}

- (void)addTestDataToRealmDirectly {
    RLMRealm *realm = [RLMRealm defaultRealm];
    
    RealmToDo *firstToDo = [[RealmToDo alloc] initWithToDoText:@"first"];
    RealmToDo *secondToDo = [[RealmToDo alloc] initWithToDoText:@"second"];
    RealmToDo *thirdToDo = [[RealmToDo alloc] initWithToDoText:@"third"];
    
    // In main, the creation primary key is the seconds from the timeIntervalSince1970.
    // To avoid waiting, manually set the creation primary key.
    firstToDo.creation = 1;
    secondToDo.creation = 2;
    thirdToDo.creation = 3;
    
    [realm beginWriteTransaction];
    [realm addObject:firstToDo];
    [realm addObject:secondToDo];
    [realm addObject:thirdToDo];
    [realm commitWriteTransaction];
}

@end
