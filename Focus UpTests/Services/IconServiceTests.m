//
//  IconServiceTests.m
//  Focus UpTests
//
//  Created by Power, Jean on 11/19/20.
//  Copyright © 2020 Power, Jean. All rights reserved.
//

#import <XCTest/XCTest.h>
#import "IconService.h"
#import "StorageServiceMock.h"
#import "NetworkServiceMock.h"

@interface IconService()

- (instancetype) initWithNetworkService:(NetworkService *)networkService storageService:(StorageService *) storageService;

@end

@interface IconServiceTests : XCTestCase

@property StorageServiceMock *storageServiceMock;
@property NetworkServiceMock *networkServiceMock;
@property IconService *sut;

@end

@implementation IconServiceTests

- (void)setUp {
    self.storageServiceMock = [[StorageServiceMock alloc] init];
    self.networkServiceMock = [[NetworkServiceMock alloc] init];
    self.sut = [[IconService alloc] initWithNetworkService:self.networkServiceMock storageService:self.storageServiceMock];
}

- (void)tearDown {
    self.storageServiceMock = nil;
    self.networkServiceMock = nil;
    self.sut = nil;
}

- (void)testIfIconsAreNotFetchedThenPlaceholdersAreReturned {
    UIImage *addPlaceholder = [UIImage systemImageNamed:@"plus"];
    UIImage *checkedPlaceholder = [UIImage systemImageNamed:@"square"];
    UIImage *uncheckedPlaceholder = [UIImage systemImageNamed:@"checkmark.square"];
    
    UIImage *returnedAddImage = [self.sut imageForIcon:IconTypeAdd];
    UIImage *returnedCheckedImage = [self.sut imageForIcon:IconTypeChecked];
    UIImage *returnedUncheckedImage = [self.sut imageForIcon:IconTypeUnchecked];
    
    XCTAssertTrue([self imagesMatch:addPlaceholder secondImage:returnedAddImage]);
    XCTAssertTrue([self imagesMatch:checkedPlaceholder secondImage:returnedCheckedImage]);
    XCTAssertTrue([self imagesMatch:uncheckedPlaceholder secondImage:returnedUncheckedImage]);
}

- (void)testIfIconsAreFetchedThenIconsAreReturned {
    UIImage *fetchedImage = [self dataFromSystemImage];
    self.networkServiceMock.returnableData = UIImagePNGRepresentation(fetchedImage);

    [self.sut prefetchIcons];
    
    UIImage *returnedAddImage = [self.sut imageForIcon:IconTypeAdd];
    UIImage *returnedCheckedImage = [self.sut imageForIcon:IconTypeChecked];
    UIImage *returnedUncheckedImage = [self.sut imageForIcon:IconTypeUnchecked];
    
    XCTAssertTrue([self imagesMatch:fetchedImage secondImage:returnedAddImage]);
    XCTAssertTrue([self imagesMatch:fetchedImage secondImage:returnedCheckedImage]);
    XCTAssertTrue([self imagesMatch:fetchedImage secondImage:returnedUncheckedImage]);
}

- (BOOL)imagesMatch:(UIImage *) firstimage secondImage:(UIImage *) secondImage {
    return [UIImagePNGRepresentation(firstimage) isEqualToData:UIImagePNGRepresentation(secondImage)];
}

// The NSData underlying a `UIImage` created from `systemImageNamed` versus `imageWithData` for the same image is different.
// To ensure consistent `UIImage` data returned from the `NetworkServiceMock`, we convert a `UIImage' from `systemImageNamed` to `NSData` and load this into a `UIImage` for use.
- (UIImage *)dataFromSystemImage {
    UIImage *systemImage = [UIImage systemImageNamed:@"rectangle"];
    NSData *systemImageAsData = UIImagePNGRepresentation(systemImage);
    return [UIImage imageWithData:systemImageAsData];
}

@end

