//
//  StorageServiceTests.m
//  Focus UpTests
//
//  Created by Power, Jean on 11/19/20.
//  Copyright © 2020 Power, Jean. All rights reserved.
//

#import <XCTest/XCTest.h>
#import "FileManagerMock.h"
#import "StorageService.h"

@interface StorageService()

- (instancetype)initWithFileManager:(NSFileManager *)fileManager cache:(NSCache *) cache;

@end

@interface StorageServiceTests : XCTestCase
    
@property NSCache *cache;
@property FileManagerMock *fileManagerMock;
@property StorageService *sut;
@property NSData *dataToStore;
@property NSString *identifier;

@end

@implementation StorageServiceTests

- (void)setUp {
    self.cache = [[NSCache alloc] init];
    self.fileManagerMock = [[FileManagerMock alloc] init];
    self.sut = [[StorageService alloc] initWithFileManager:self.fileManagerMock cache:self.cache];
    self.dataToStore = [@"Test data to store" dataUsingEncoding:NSUTF8StringEncoding];
    self.identifier =  @"Test ID";
}
             
- (void)tearDown {
    self.cache = nil;
    self.fileManagerMock = nil;
    self.sut = nil;
}

- (void)testDataIsPresentIfStored {
    XCTAssertFalse([self.sut dataPresentForIdentifier:self.identifier]);
    [self.sut storeData:self.dataToStore withIdentifier:self.identifier];
    XCTAssertTrue([self.sut dataPresentForIdentifier:self.identifier]);
}

- (void)testDataIsCached {
    XCTAssertNil([self.cache objectForKey:self.identifier]);
    [self.sut storeData:self.dataToStore withIdentifier:self.identifier];
    XCTAssertNotNil([self.cache objectForKey:self.identifier]);
    XCTAssertEqual(self.dataToStore, [self.cache objectForKey:self.identifier]);
}

- (void)testIfDataInCacheThenCachedFileReturned {
    [self.sut storeData:self.dataToStore withIdentifier:self.identifier];
    XCTAssertEqual(self.dataToStore, [self.sut fetchDataWithIdentifier:self.identifier]);
}

- (void)testIfDataNotInCacheThenPersistedFileReturned {
    [self.sut storeData:self.dataToStore withIdentifier:self.identifier];
    [self.cache removeAllObjects];
    XCTAssertNil([self.sut fetchDataWithIdentifier:self.identifier]);
    
    // Add returnable data to the mock file system
    self.fileManagerMock.dataAtPath = self.dataToStore;
    
    XCTAssertEqualObjects(self.dataToStore, [self.sut fetchDataWithIdentifier:self.identifier]);
}

@end

