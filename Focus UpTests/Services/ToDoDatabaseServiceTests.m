//
//  ToDoDatabaseServiceTests.m
//  Focus UpTests
//
//  Created by Power, Jean on 11/19/20.
//  Copyright © 2020 Power, Jean. All rights reserved.
//

#import <XCTest/XCTest.h>
#import <Realm.h>
#import "RealmToDo.h"
#import "ToDoDatabaseService.h"

@interface ToDoDatabaseServiceTests : XCTestCase

@property ToDoDatabaseService *sut;

@end

@implementation ToDoDatabaseServiceTests

- (void)setUp {
    // As per Realm documentation:
    // Use an in-memory Realm identified by the name of the current test.
    // This ensures that each test can't accidentally access or modify the data
    // from other tests or the application itself, and because they're in-memory,
    // there's nothing that needs to be cleaned up.
    RLMRealmConfiguration *config = [RLMRealmConfiguration defaultConfiguration];
    config.inMemoryIdentifier = self.name;
    [RLMRealmConfiguration setDefaultConfiguration:config];
    self.sut = [[ToDoDatabaseService alloc] init];
}

- (void)testPutStoresToDo {
    RealmToDo *realmToDo = [[RealmToDo alloc] initWithToDoText:@"I am some text for the testing of the database"];
    
    [self.sut put:realmToDo];
    
    RLMResults<RealmToDo *> *returnedToDos = self.sut.fetchAllSorted;
    
    XCTAssertEqualObjects(@"I am some text for the testing of the database", returnedToDos[0].toDoText);
}

- (void)testFetchReturnsOrderedResultsByElementCreation {
    RealmToDo *firstToDo = [[RealmToDo alloc] initWithToDoText:@"first"];
    RealmToDo *secondToDo = [[RealmToDo alloc] initWithToDoText:@"second"];
    RealmToDo *thirdToDo = [[RealmToDo alloc] initWithToDoText:@"third"];
    
    // In main, the creation primary key is the seconds from the timeIntervalSince1970.
    // To avoid waiting, manually set the creation primary key.
    firstToDo.creation = 1;
    secondToDo.creation = 2;
    thirdToDo.creation = 3;
    
    [self.sut put:secondToDo];
    [self.sut put:thirdToDo];
    [self.sut put:firstToDo];
    
    RLMResults<RealmToDo *> *returnedToDos = self.sut.fetchAllSorted;

    XCTAssertEqualObjects(@"first", returnedToDos[0].toDoText);
    XCTAssertEqualObjects(@"second", returnedToDos[1].toDoText);
    XCTAssertEqualObjects(@"third", returnedToDos[2].toDoText);
}

- (void)testTogglingUncompletedToDoCompletesToDo {
    RealmToDo *realmToDo = [[RealmToDo alloc] initWithToDoText:@"Test"];
    realmToDo.completed = NO;
    
    [self.sut put:realmToDo];
    [self.sut toggleCompletionOfToDo:realmToDo];
    
    RLMResults<RealmToDo *> *returnedToDos = self.sut.fetchAllSorted;
    XCTAssertTrue(returnedToDos[0].completed);
}

- (void)testTogglingCompletedToDoUncompletesToDo {
    RealmToDo *realmToDo = [[RealmToDo alloc] initWithToDoText:@"Test"];
    realmToDo.completed = YES;
    
    [self.sut put:realmToDo];
    [self.sut toggleCompletionOfToDo:realmToDo];
    
    RLMResults<RealmToDo *> *returnedToDos = self.sut.fetchAllSorted;
    
    XCTAssertFalse(returnedToDos[0].completed);
}

- (void)testDeleteRemovesToDo {
    RealmToDo *realmToDo = [[RealmToDo alloc] initWithToDoText:@"Test"];
    
    [self.sut put:realmToDo];
    [self.sut delete:realmToDo];
    
    RLMResults<RealmToDo *> *returnedToDos = self.sut.fetchAllSorted;
    XCTAssertEqual(0, returnedToDos.count);
}

@end
