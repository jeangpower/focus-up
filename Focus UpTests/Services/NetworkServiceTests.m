//
//  NetworkServiceTests.m
//  Focus UpTests
//
//  Created by Power, Jean on 11/19/20.
//  Copyright © 2020 Power, Jean. All rights reserved.
//

#import <XCTest/XCTest.h>
#import "NSURLSessionMock.h"
#import "NetworkService.h"

@interface NetworkService()

- (instancetype)initWithURLSession:(NSURLSession *)urlSession;

@end

@interface NetworkServiceTests : XCTestCase

@end

@implementation NetworkServiceTests

- (void)testErrorIsReturnedIfErrorFromService {
    NSURLSessionMock *mock = [[NSURLSessionMock alloc] init];
    mock.returnableError = [[NSError alloc] initWithDomain:NSURLErrorDomain code:1111 userInfo:nil];
    NetworkService *sut = [[NetworkService alloc] initWithURLSession:mock];
    
    [sut fetchDataFromURL:[[NSURL alloc] init] completionHandler:^(NSData *data, NSError *error) {
        XCTAssertEqual(1111, error.code);
        XCTAssertEqual(NSURLErrorDomain, error.domain);
        XCTAssertNil(data);
    }];
    
}

- (void)testDataIsReturnedIfDataIsFetched {
    NSURLSessionMock *mock = [[NSURLSessionMock alloc] init];
    mock.returnableData = [@"Some test data" dataUsingEncoding:NSUTF8StringEncoding];
    NetworkService *sut = [[NetworkService alloc] initWithURLSession:mock];
    
    [sut fetchDataFromURL:[[NSURL alloc] init] completionHandler:^(NSData *data, NSError *error) {
        NSString *returnedDataAsString = [[NSString alloc] initWithData:data encoding:NSUTF8StringEncoding];
        XCTAssertNil(error);
        XCTAssertEqualObjects(@"Some test data", returnedDataAsString);
    }];
}

@end
