//
//  IconTests.m
//  Focus UpTests
//
//  Created by Power, Jean on 11/19/20.
//  Copyright © 2020 Power, Jean. All rights reserved.
//

#import <XCTest/XCTest.h>
#import "Icon.h"

@interface IconTests : XCTestCase

@end

@implementation IconTests

- (void)testIconInstantiatedWithData {
    Icon *sut = [[Icon alloc] initWithIconType:IconTypeChecked url:[NSURL URLWithString:@"www.test.com"] identifier:@"TestID" placeholder:@"Some text"];
    XCTAssertEqualObjects(@"TestID", sut.identifier);
    XCTAssertEqualObjects(@"Some text", sut.placeholder);
    XCTAssertEqualObjects(@"www.test.com", sut.url.absoluteString);
    XCTAssertEqual(IconTypeChecked, sut.iconType);
}


@end
