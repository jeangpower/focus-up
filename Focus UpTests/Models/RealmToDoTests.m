//
//  RealmToDoTests.m
//  Focus UpTests
//
//  Created by Power, Jean on 11/20/20.
//  Copyright © 2020 Power, Jean. All rights reserved.
//

#import <XCTest/XCTest.h>
#import <Realm.h>
#import "RealmToDo.h"

@interface RealmToDoTests : XCTestCase

@end

@implementation RealmToDoTests
- (void)testRealmToDoInstantiatedWithData {
    RealmToDo *toDo = [[RealmToDo alloc] initWithToDoText:@"I am some text"];
    XCTAssertEqualObjects(@"I am some text", toDo.toDoText);
}

- (void)testRealmToDoInstantiatedAsUncompleted {
    RealmToDo *toDo = [[RealmToDo alloc] initWithToDoText:@"I am some text"];
    XCTAssertFalse(toDo.completed);
}

- (void)testRealmToDoInstantiatedAPrimaryKey {
    RealmToDo *toDo = [[RealmToDo alloc] initWithToDoText:@"I am some text"];
    // Is not equal to default for int
    XCTAssertTrue(toDo.creation != 0);
}

@end
