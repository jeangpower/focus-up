# **Focus Up**


### One To Do list to rule them all - focused, clean, simple


- To add a new To Do, click on the plus icon, enter your To Do, and tap "Done".
- To delete a To Do, swipe.
- To complete a To Do, tap. 

### **Next steps/Improvements** 
1. Expand unit tests; currently focused on public APIs. Consider more edge cases and side effects, add UI tests for Views.
2. IconService loads data each time into UIImage in `imageForIcon`, called from `cellForRowAtIndexPath`; no performance impact (up to 50 rows) as the icons are tiny, but consider creating these once, storing and returning the same instance each time.
3. Synchronously fetching all To Do at `viewDidLoad` is not scalable and something like pagination should be put in place.
4. Don't reload `UITableView` each time; consider hand-rolled/Realm's concept of "changes", and reloading just the rows that changed.
5. Implement a better `enum` for IconType to expose it as a finite collection - this could then be used to ensure main/tests fail to compile if new icons are added to the `enum` but not handled, and remove some duplication between main and tests.
6. Not good practice to add main files to the test target; better to work around the Host Application issue with a mocked SceneDelegate and AppDelegate.
7. Investigate images created using `systemImageNamed` returning different `NSData` to images created using  `imageWithData` with the same image (`IconServiceTests`)
8. Consider Realm dependency mock; because `RLMResults` cannot be initialised and notifications for changes are tied to this collection, classes which depend on Realm are deeply coupled to Realm for testing. This introduces flakiness to the unit tests, race conditions and impacts what tests can be completed. 
9. Consider primary key for Realm. Currently using seconds `timeIntervalSince1970`, but this can have conflicts if using synced databases across devices, and also causes issues with testing (Tests can add to the database in milliseconds, duplicating primary keys). A hash of the text & timeIntervalSince1970 may be appropriate.
8. Full accessibility review - eg. use VoiceOver and screen curtain to ensure flow is clear and reasonable, set all text to Dynamic Type and confirm functionality with different text sizes.
9. Remove Pods from workspace; added to prevent dependency manager clashes.
10. Currently no authentication required to fetch icons. For a large scale app, this would need to be put in place to prevent issues (eg. DDoS on backend)
11. Localise text, depending on app distribution.
12. Sanitise To-Do text, depending on Realm's functionality (eg. emoji)


## **Attribution**

Add, Checked and Unchecked Icons [David Gandy](https://www.flaticon.com/authors/dave-gandy)

App Icon by [Vitaly Gorbachev](https://www.flaticon.com/authors/vitaly-gorbachev)

## **Xcode version**
Version 11.3.1

